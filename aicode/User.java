
public class User {
	
String userName = "";
int totalBudget;
float averageBudget;
int playerCount;
String[] rosterRoles = {"QB1", "RB1", "RB2", "WR1", "WR2", "WR3", "TE", "Flex"};
String[] rosterPlayers = {"",   "",    "",    "",    "",    "",    "",   ""};

	
	public User(String name, int budget, int count)
	{
		userName = name;
		totalBudget = budget;
		averageBudget = budget/count;
	}
	
	public String getName()
	{
		return userName;
	}
	
	public int getBudget()
	{
		return totalBudget;
	}

	public void setBudget(int cost)
	{
		//System.out.print("Current(" + userBudget + ") - Player Cost(" + cost + ")\n");
		totalBudget -= cost;
	}
	
	public void setTeam(Player drafted, int role)
	{
		rosterPlayers[role] = drafted.getName();
	}
	
	public void printTeam()
	{
		for(int i = 0; i < rosterPlayers.length; i++)
		{
			System.out.println(rosterPlayers[i]);
		}
	}
	
}
