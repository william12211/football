import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Driver {

	public static void main(String[] args) throws IOException {
				
		String compare = null;
		
		Pattern name = Pattern.compile("[a-zA-Z]+[^\\w]+[a-zA-Z]*");
		Pattern position = Pattern.compile("[a-zA-Z]{2}");	
		
		int count = 0;
		
		try (BufferedReader br = new BufferedReader(new FileReader("2015_Week1.txt"))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		       //Code Here
		    	
		    	compare = line;
				Matcher m = name.matcher(compare);
				
		    	
		    	if(m.find())
		    	{
		    	    System.out.println(compare.substring(m.start(), m.end()));
		    	}
		    }
		}
		
		User u1 = new User("Tanner Detlef", 50000, 8);
				
		Player[] RunningBack = new Player[2]; 
		Player[] Quarterback = new Player[2]; 		
		
		Player p1 = new Player("Tom Brady", "QB", 29.5, 7000);
		Player p2 = new Player("Matt Ryan", "QB", 32.5, 6500);
		
		Player r1 = new Player("Jonathan Stewart", "RB", 21.3, 5500);
		Player r2 = new Player("DeAngelo Williams", "RB", 5.3, 6000);
			
		Quarterback[0] = p1;
		Quarterback[1] = p2;
		
		RunningBack[0] = r1;
		RunningBack[1] = r2;
		
		Prediction calc = new Prediction();
		
		int temp = calc.BestValue("QB", Quarterback, u1);
		int cost = Quarterback[temp].getCost();
		u1.setBudget(cost);
		
		System.out.println("Your budget is now: " + u1.getBudget());
		u1.setTeam(Quarterback[temp], 0);
		
		temp = calc.BestValue("RB", RunningBack, u1);
		cost = RunningBack[temp].getCost();
		u1.setBudget(cost);
	
		System.out.println("Your budget is now: " + u1.getBudget());
		u1.setTeam(RunningBack[temp], 1);
		
		System.out.println("\nYour current team is: ");
		u1.printTeam();
		
	}

}
