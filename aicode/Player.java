
public class Player {
	
String playerName = "";
String playerPosition = "";
double playerValue;
int playerCost;


	public Player(String name, String pos, double value, int cost)
	{
		playerName = name;
		playerPosition = pos;
		playerValue = value;
		playerCost = cost;
	}

	public String getName()
	{
		return playerName;
	}
	
	public void setName(String name)
	{
		playerName = name;
	}

	public String getPosition()
	{
		return playerPosition;
	}
	
	public double getValue()
	{
		return playerValue;
	}
	
	public int getCost()
	{
		return playerCost;
	}


}
